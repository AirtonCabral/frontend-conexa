import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex);

const baseUrl = 'http://desafio.conexasaude.com.br/';
const loginUrl = 'api/login';
const consultListUrl = 'api/consultas';
const idConsult =  'api/consulta/'


export const store =  new Vuex.Store({
    state: {
      user: {
        nome: null,
        email: null,
        token: null,
      },
      consultList:{},
      consultIDDetail :{}
    },
    mutations: {
      mutCurrentUser(state, payload) {
        state.user = payload;
      },
      mutConsultList(state, payload) {
        state.consultList = payload;
      },
      mutConsultIDDetail(state, payload) {
        state.consultIDDetail = payload;
      }
    },
    actions: {
      async setCurrentUser(state, email, password) {
        await axios.post(baseUrl + loginUrl, { 
          email: email,
          senha: password
        }).then( response => {
          // eslint-disable-next-line no-debugger
          debugger
          console.log(response.data.data)
          state.commit('mutCurrentUser', response.data.data)
          sessionStorage.setItem(`token`, response.data.data.token);
        }).catch((error) => {
          console.log('error ' + error);
          this.$router.push('/Login') 
        })
        return 
      },
      async getConsultList(state){
        // eslint-disable-next-line no-debugger
        debugger
        const token = sessionStorage.getItem(`token`);
        await axios.get(baseUrl + consultListUrl, { headers: {
          Authorization: 'Bearer '+ token
          }
        }).then(response => {
          console.log(response.data.data);
          state.commit('mutConsultList', response.data.data)
        }).catch((error) => {
          console.log('error ' + error);
          this.$router.push('/Login') 
        })
      },
      async getConsultIDDetail(state, obj){
        // eslint-disable-next-line no-debugger
        debugger
        const token = sessionStorage.getItem(`token`);
        await axios.get(baseUrl + idConsult + obj.id, { headers: {
          Authorization: 'Bearer '+ token
          }
        }).then(response => {
          console.log(response.data.data);
          state.commit('mutConsultIDDetail', response.data.data)
        }).catch((error) => {
          console.log('error ' + error);
        })
      },
      async createConsult(state, obj) {
        // eslint-disable-next-line no-debugger
        debugger
        const token = sessionStorage.getItem(`token`);
        await axios.post( baseUrl + idConsult,
        { 
          dataConsulta: obj.dataConsulta.toJSON(),
          idMedico: obj.idMedico,
          observacao: obj.observacao,
          paciente: obj.paciente
        },{ headers: {
          Authorization: 'Bearer '+ token
          }
        }).then( response => {
          console.log(response.data.data)
          alert('Adicionado com sucesso')
        }).catch((error) => {
          console.log('error ' + error);
          alert('Nao pode ser adicionado')
        })
        return 
      },
    },
    getters: {
      user: state => state.user,
      consultList: state => state.consultList,
      consultIDDetail: state => state.consultIDDetail
    },
    modules: {}
});

