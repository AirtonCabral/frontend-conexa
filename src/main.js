import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import DatetimePicker from 'vuetify-datetime-picker'

import "../node_modules/vuetify/dist/vuetify.min.css"

import Home from './views/Home'
import Login from './views/Login'
import Consulta from './views/Consulta'
import CriarConsulta from './views/CriarConsulta'

import { store } from './store'


Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(DatetimePicker)

const router = new VueRouter({
  routes: [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },{
    path: '/Consulta',
    name: 'Consulta',
    component: Consulta
  },{
    path: '/Criar',
    name: 'Criar',
    component: CriarConsulta
  }]
});

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')